/*
Contains most of the procs that are called when a mob is attacked by something

bullet_act
ex_act
meteor_act
emp_act

*/

/mob/living/carbon/human/bullet_act(var/obj/item/projectile/P, var/def_zone)

	if(wear_suit && istype(wear_suit, /obj/item/clothing/suit/armor/laserproof))
		if(istype(P, /obj/item/projectile/energy) || istype(P, /obj/item/projectile/beam))
			var/reflectchance = 40 - round(P.damage/3)
			if(!(def_zone in list("chest", "groin")))
				reflectchance /= 2
			if(prob(reflectchance))
				visible_message("\red <B>The [P.name] gets reflected by [src]'s [wear_suit.name]!</B>")

				// Find a turf near or on the original location to bounce to
				if(P.starting)
					var/new_x = P.starting.x + pick(0, 0, 0, 0, 0, -1, 1, -2, 2)
					var/new_y = P.starting.y + pick(0, 0, 0, 0, 0, -1, 1, -2, 2)
					var/turf/curloc = get_turf(src)

					// redirect the projectile
					P.original = locate(new_x, new_y, P.z)
					P.starting = curloc
					P.current = curloc
					P.firer = src
					P.yo = new_y - curloc.y
					P.xo = new_x - curloc.x

				return -1 // complete projectile permutation
	if(istype(P, /obj/item/projectile/energy/electrode))
		if(src.stat == DEAD)
			if(prob(30) && (src.timeofdeath + 1800 > world.time) && !(NOCLONE in src.mutations))
				src.stat = UNCONSCIOUS
				src.visible_message( \
					"\red [src]'s body trembles!", \
					"\red You feel the life enter your body with the heavy pain!", \
					"\red You hear a heavy electric crack!" \
				)
			else
				src.visible_message( \
					"\red [src]'s lifeless body trembles!", \
					"\red A discharge of electricity passes through your body, but the efforts to bring you back to life are useless.", \
					"\red You hear a heavy electric crack!" \
				)


	if(check_shields(P.damage, "the [P.name]"))
		P.on_hit(src, 2)
		return 2

	var/obj/item/weapon/cloaking_device/C = locate((/obj/item/weapon/cloaking_device) in src)
	if(C && C.active)
		C.attack_self(src)//Should shut it off
		update_icons()
		src << "\blue Your [C.name] was disrupted!"
		Stun(2)

	if(istype(equipped(),/obj/item/device/assembly/signaler))
		var/obj/item/device/assembly/signaler/signaler = equipped()
		if(signaler.deadman && prob(80))
			src.visible_message("\red [src] triggers their deadman's switch!")
			signaler.signal()


		var/datum/organ/external/organ = get_organ(check_zone(def_zone))

		var/armor = checkarmor(organ, "bullet")

		if((P.embed && prob(20 + max(P.damage - armor, -10))) && P.damage_type == BRUTE)
			var/obj/item/weapon/shard/shrapnel/SP = new()
			(SP.name) = "[P.name] shrapnel"
			(SP.desc) = "[SP.desc] It looks like it was fired from [P.shot_from]."
			(SP.loc) = organ
			organ.implants += SP
			visible_message("<span class='danger'>The projectile sticks in the wound!</span>")
			src.verbs += /mob/proc/yank_out_object
			SP.add_blood(src)

	return (..(P , def_zone))


/mob/living/carbon/human/getarmor(var/def_zone, var/type)
	var/armorval = 0
	var/organnum = 0

	if(def_zone)
		if(isorgan(def_zone))
			return checkarmor(def_zone, type)
		var/datum/organ/external/affecting = get_organ(ran_zone(def_zone))
		return checkarmor(affecting, type)
		//If a specific bodypart is targetted, check how that bodypart is protected and return the value.

	//If you don't specify a bodypart, it checks ALL your bodyparts for protection, and averages out the values
	for(var/datum/organ/external/organ in organs)
		armorval += checkarmor(organ, type)
		organnum++
	return (armorval/max(organnum, 1))


/mob/living/carbon/human/proc/checkarmor(var/datum/organ/external/def_zone, var/type)
	if(!type)	return 0
	var/protection = 0
	var/list/body_parts = list(head, wear_mask, wear_suit, w_uniform)
	for(var/bp in body_parts)
		if(!bp)	continue
		if(bp && istype(bp ,/obj/item/clothing))
			var/obj/item/clothing/C = bp
			if(C.body_parts_covered & def_zone.body_part)
				protection += C.armor[type]
	return protection

/mob/living/carbon/human/proc/check_head_coverage()

	var/list/body_parts = list(head, wear_mask, wear_suit, w_uniform)
	for(var/bp in body_parts)
		if(!bp)	continue
		if(bp && istype(bp ,/obj/item/clothing))
			var/obj/item/clothing/C = bp
			if(C.body_parts_covered & HEAD)
				return 1
	return 0

/mob/living/carbon/human/proc/check_shields(var/damage = 0, var/attack_text = "the attack")
	if(l_hand && istype(l_hand, /obj/item/weapon))//Current base is the prob(50-d/3)
		var/obj/item/weapon/I = l_hand
		if(I.IsShield() && (prob(50 - round(damage / 3))))
			visible_message("\red <B>[src] blocks [attack_text] with the [l_hand.name]!</B>")
			return 1
	if(r_hand && istype(r_hand, /obj/item/weapon))
		var/obj/item/weapon/I = r_hand
		if(I.IsShield() && (prob(50 - round(damage / 3))))
			visible_message("\red <B>[src] blocks [attack_text] with the [r_hand.name]!</B>")
			return 1
	if(wear_suit && istype(wear_suit, /obj/item/))
		var/obj/item/I = wear_suit
		if(I.IsShield() && (prob(35)))
			visible_message("\red <B>The reactive teleport system flings [src] clear of [attack_text]!</B>")
			var/list/turfs = new/list()
			for(var/turf/T in orange(6))
				if(istype(T,/turf/space)) continue
				if(T.density) continue
				if(T.x>world.maxx-6 || T.x<6)	continue
				if(T.y>world.maxy-6 || T.y<6)	continue
				turfs += T
			if(!turfs.len) turfs += pick(/turf in orange(6))
			var/turf/picked = pick(turfs)
			if(!isturf(picked)) return
			src.loc = picked
			return 1
	return 0

/mob/living/carbon/human/emp_act(severity)
	for(var/obj/O in src)
		if(!O)	continue
		O.emp_act(severity)
	for(var/datum/organ/external/O  in organs)
		if(O.status & ORGAN_DESTROYED)	continue
		O.emp_act(severity)
		for(var/datum/organ/internal/I  in O.internal_organs)
			if(I.robotic == 0)	continue
			I.emp_act(severity)
	..()


/mob/living/carbon/human/proc/attacked_by(var/obj/item/I, var/mob/living/user, var/def_zone)
	if(!I || !user)	return 0

	var/target_zone = def_zone? check_zone(def_zone) : get_zone_with_miss_chance(user.zone_sel.selecting, src)

	if(user == src) // Attacking yourself can't miss
		target_zone = user.zone_sel.selecting
	if(!target_zone)
		visible_message("\red <font size=1><B>[user] ������������&#255; [I] �� [src]!</font>") // Translate it 3261
		return 0

	var/datum/organ/external/affecting = get_organ(target_zone)
	if (!affecting)
		return 0
	if(affecting.status & ORGAN_DESTROYED)
		user << "What [affecting.display_name]?"
		return 0
	var/hit_area = affecting.display_name

	if((user != src) && check_shields(I.force, "the [I.name]"))
		return 0

	if(istype(I,/obj/item/weapon/card/emag))
		if(!(affecting.status & ORGAN_ROBOT))
			user << "\red That limb isn't robotic."
			return
		if(affecting.sabotaged)
			user << "\red [src]'s [affecting.display_name] is already sabotaged!"
		else
			user << "\red You sneakily slide [I] into the dataport on [src]'s [affecting.display_name] and short out the safeties."
			var/obj/item/weapon/card/emag/emag = I
			emag.uses--
			affecting.sabotaged = 1
		return 1
/*
	if(I.attack_verb.len)
		visible_message("\red <B>[src] has been [pick(I.attack_verb)] in the [hit_area] with [I.name] by [user]!</B>")
	else
		visible_message("\red <B>[src] has been attacked in the [hit_area] with [I.name] by [user]!</B>")
*/


	if(!src.stat && src.blockMode==1) // 0 - dodge ; 1 - parry\block
		if(istype(src.r_hand,/obj/item) || istype(src.l_hand,/obj/item))
			if(prob(src.r_hand:chance_parry-round(user.agility/3)))
				if(prob(user.strength))
					visible_message("\red \b [user] ������� [user.gender=="MALE"?"����":"������"]. [user.gender=="MALE"?"��":"���"] ��������� ���� [src]!</font>")
					return
				visible_message("\red \b <font size=1>[src.name] [pick("�����","�����")] �������� ����� � ������� [src.r_hand.name]!</font>")
				return
			if(prob(src.l_hand:chance_parry-round(user.agility/3)))
				if(prob(user.strength))
					visible_message("\red \b <font size=1>[user] ������� [user.gender=="MALE"?"����":"������"]. [user.gender=="MALE"?"��":"���"] ��������� ���� [src]!</font>")
					return
				visible_message("\red \b <font size=1>[src.name] [pick("�����","�����")] �������� ����� � ������� [src.r_hand.name]!</font>")
				return

	var/possible_damage = I.force
// TODO ��������� �������� �� ������

	var/mod_knockdown = 0 // ������� - ��� ����� ������ ������� �������� ������� ����, � ����� �����, ����� �����������, ����� �� ���������� ��� ��� ���.
	var/mod_knockout = 0 // ������ - ��� ����� ������ ��� ������� ��������, ������ �� ����� � �� ��������.
	var/mod_stun = 0
	var/mod_weaken = 0
	var/mod_disarm = 0

	switch(I.w_type)
		if("stabbing")	// �������
			visible_message("\red <font size=2><B>[user] ��������� ����� � [zone_russian(hit_area)] [src]!</B></font>")
			mod_disarm = I.modifier_disarm + 15
			mod_stun = I.modifier_stun + 5
			mod_weaken = I.modifier_weaken + 5
//			world << "�������"

		if("cutting")	// �������
			if(I.special_name)
				visible_message("\red <font size=2><B>[user] ��������� [I.special_name] [hit_area] [src]!</B></font>")
			visible_message("\red <font size=2><B>[user] � ������� [I.name] ��������� [hit_area] [src]!</B></font>")
			mod_disarm = I.modifier_disarm + 10
			mod_stun = I.modifier_stun + 5
			mod_weaken = I.modifier_weaken + 10
//			world << "�������"

		if("slashing")	// �������
			visible_message("\red <font size=2><B>[user] ������������� [I.name] ���� � [hit_area] [src]!</B></font>")
			mod_disarm = I.modifier_disarm + 5
			mod_stun = I.modifier_stun + 10
			mod_weaken = I.modifier_weaken + 15
//			world << "�������"

		if("blunt")		// ��������
			visible_message("\red <font size=2><B>[user] ���������� [I.name] ���� [src] � [hit_area]!</B></font>")
			mod_disarm = I.modifier_disarm + 5
			mod_stun = I.modifier_stun + 15
			mod_weaken = I.modifier_weaken + 10
//			world << "��������"

		if("none")		// �� ������
			if(I.special_name)
				visible_message("\red <font size=2><B>[user] ���� [I.special_name] � [hit_area] [src]!</B></font>")
			visible_message("\red <font size=2><B>[user] ���������&#255; [I.name] ���� [src] � [hit_area]!</B></font>")
			mod_disarm = I.modifier_disarm + 10
//			world << "�� ������"

	switch(target_zone)
		if("eyes")
			mod_weaken += 10
			mod_stun += 5
			mod_knockdown += 5
		if("head")
			mod_stun += 8
			mod_knockout += 10
			mod_weaken += 5
			mod_knockdown += src.gender=="MALE"?2:15
		if("chest")
			mod_stun += 8
			mod_knockout += 10
			mod_weaken += 5
			mod_knockdown += src.gender=="MALE"?5:20
		if("right arm","r_arm")
			mod_disarm += 15
			mod_weaken += 2
		if("left arm","l_arm")
			mod_disarm += 15
			mod_weaken += 5
		if("right hand","r_hand")
			mod_disarm += 15
			mod_weaken += 5
		if("left hand","l_hand")
			mod_disarm += 15
			mod_weaken += 5
		if("groin")
			mod_stun = 15
			mod_knockout = 10
			mod_weaken = 5
			mod_knockdown = src.gender=="MALE"?20:5
		if("right leg","r_leg")
			mod_weaken += 10
			mod_stun += 2
		if("left leg","l_leg")
			mod_weaken += 10
			mod_stun += 2
		if("right foot","r_foot")
			mod_weaken += 10
			mod_stun += 2
		if("left foot","l_foot")
			mod_weaken += 10
			mod_stun += 2

	var/armor = run_armor_check(affecting, "melee", "���&#255; ����&#255; �������� ���[hit_area=="chest"?"�":"�"] [zone_russian(hit_area)].", "���&#255; ����&#255; ��&#255;����� ���� � [zone_russian(hit_area)].")
	if(armor >= 2)	return 0
	possible_damage += round(user.strength/15) - round(src.endurance/15)

	if(possible_damage < 0)	possible_damage = 1

// prob( goes there
	if(prob(mod_knockdown))
		apply_effect(round(possible_damage/5), STUN, armor)
		shake_camera(src,10,1)
//		world << "mod_knockdown ������"

	if(prob(mod_knockout))
		apply_effect(possible_damage, WEAKEN, armor)
		shake_camera(src,20,3)
//		world << "mod_knockout ������"

	if(prob(mod_stun))
		apply_effect(round(mod_stun/10), STUN, armor)
//		world << "mod_stun ������"

	if(prob(mod_weaken))
		apply_effect(round(mod_weaken/10), WEAKEN, armor)
//		world << "mod_weaken ������"


	if(prob(mod_disarm))
		var/talked = 0
		var/randn = rand(1, 100)
		if(randn <= 60)
		//BubbleWrap: Disarming breaks a pull
			if(src.pulling)
				visible_message("\red <b>[user] has broken [src]'s grip on [src.pulling]!</B>") // Translate it 3243
				talked = 1
				src.stop_pulling()

		if(!talked)	//BubbleWrap
			src.drop_item()
			visible_message("\red <B>[user] has disarmed [src]!</B>") // Translate it 3246
			playsound(loc, 'sound/weapons/thudswoosh.ogg', 50, 1, -1)

//		world << "mod_disarm ������"
// End prob( fun

/*
	if(I.attack_verb.len)
		visible_message("\red <B>[src] has been [pick(I.attack_verb)] in the [hit_area] with [I.name] by [user]!</B>")
	else
		visible_message("\red <B>[src] has been attacked in the [hit_area] with [I.name] by [user]!</B>")
*/

	if(!possible_damage)	return 0

	var/weapon_sharp = is_sharp(I)
	var/weapon_edge = has_edge(I)
	if ((weapon_sharp || weapon_edge) && prob(getarmor(target_zone, "melee")))
		weapon_sharp = 0
		weapon_edge = 0

	apply_damage(possible_damage, I.damtype, affecting, armor , sharp=weapon_sharp, edge=weapon_edge, used_weapon=I)
//	apply_damage(possible_damage, I.damtype, affecting, armor , is_sharp(I), I)



/* OLD
	var/armor = run_armor_check(affecting, "melee", "Your armor has protected your [hit_area].", "Your armor has softened hit to your [hit_area].")
	var/weapon_sharp = is_sharp(I)
	var/weapon_edge = has_edge(I)
	if ((weapon_sharp || weapon_edge) && prob(getarmor(target_zone, "melee")))
		weapon_sharp = 0
		weapon_edge = 0

	if(armor >= 2)	return 0
	if(!I.force)	return 0
	var/Iforce = I.force //to avoid runtimes on the forcesay checks at the bottom. Some items might delete themselves if you drop them. (stunning yourself, ninja swords)

	apply_damage(I.force, I.damtype, affecting, armor, sharp=weapon_sharp, edge=weapon_edge, used_weapon=I)
*/


	var/bloody = 0
	if(((I.damtype == BRUTE) || (I.damtype == HALLOSS)) && prob(25 + (I.force * 2)))
		I.add_blood(src)	//Make the weapon bloody, not the person.
//		if(user.hand)	user.update_inv_l_hand()	//updates the attacker's overlay for the (now bloodied) weapon
//		else			user.update_inv_r_hand()	//removed because weapons don't have on-mob blood overlays
		if(prob(33))
			bloody = 1
			var/turf/location = loc
			if(istype(location, /turf/simulated))
				location.add_blood(src)
			if(ishuman(user))
				var/mob/living/carbon/human/H = user
				if(get_dist(H, src) <= 1) //people with TK won't get smeared with blood
					H.bloody_body(src)
					H.bloody_hands(src)

		switch(hit_area)
			if("head")//Harder to score a stun but if you do it lasts a bit longer
				if(prob(possible_damage + affecting.brute_dam + affecting.burn_dam))
					apply_effect(20, PARALYZE, armor)
					visible_message("\red <B>[src] has been knocked unconscious!</B>")
					if(src != user && I.damtype == BRUTE)
						ticker.mode.remove_revolutionary(mind)

				if(bloody)//Apply blood
					if(wear_mask)
						wear_mask.add_blood(src)
						update_inv_wear_mask(0)
					if(head)
						head.add_blood(src)
						update_inv_head(0)
					if(glasses && prob(33))
						glasses.add_blood(src)
						update_inv_glasses(0)

			if("chest")//Easier to score a stun but lasts less time
				if(prob((possible_damage + 10)))
					apply_effect(6, WEAKEN, armor)
					visible_message("\red <B>[src] has been knocked down!</B>")

				if(bloody)
					bloody_body(src)

	if(possible_damage >= 5 && prob(33))
		forcesay(hit_appends)	//forcesay checks stat already

	//Melee weapon embedded object code.
	if (I.damtype == BRUTE && !I.is_robot_module())
		var/damage = I.force
		if (armor)
			damage /= armor+1

		//blunt objects should really not be embedding in things unless a huge amount of force is involved
		var/embed_chance = weapon_sharp ? damage/I.w_class : damage/(I.w_class*3)
		var/embed_threshold = weapon_sharp ? 5*I.w_class : 15*I.w_class

		//Sharp objects will always embed if they do enough damage.
		if((weapon_sharp && damage > (10*I.w_class)) || (damage > embed_threshold && prob(embed_chance)))
			affecting.embed(I)
	return 1


//this proc handles being hit by a thrown atom
/mob/living/carbon/human/hitby(atom/movable/AM as mob|obj,var/speed = 5)
	if(istype(AM,/obj/))
		var/obj/O = AM

		if(in_throw_mode && !get_active_hand() && speed <= 5)	//empty active hand and we're in throw mode
			if(canmove && !restrained())
				if(isturf(O.loc))
					put_in_active_hand(O)
					visible_message("<span class='warning'>[src] catches [O]!</span>")
					throw_mode_off()
					return

		var/dtype = BRUTE
		if(istype(O,/obj/item/weapon))
			var/obj/item/weapon/W = O
			dtype = W.damtype
		var/throw_damage = O.throwforce*(speed/5)

		var/zone
		if (istype(O.thrower, /mob/living))
			var/mob/living/L = O.thrower
			zone = check_zone(L.zone_sel.selecting)
		else
			zone = ran_zone("chest",75)	//Hits a random part of the body, geared towards the chest

		//check if we hit
		if (O.throw_source)
			var/distance = get_dist(O.throw_source, loc)
			zone = get_zone_with_miss_chance(zone, src, min(15*(distance-2), 0))
		else
			zone = get_zone_with_miss_chance(zone, src, 15)

		if(!zone)
			visible_message("\blue \The [O] misses [src] narrowly!")
			return

		O.throwing = 0		//it hit, so stop moving

		if ((O.thrower != src) && check_shields(throw_damage, "[O]"))
			return

		var/datum/organ/external/affecting = get_organ(zone)
		var/hit_area = affecting.display_name

		src.visible_message("\red [src] has been hit in the [hit_area] by [O].")
		var/armor = run_armor_check(affecting, "melee", "Your armor has protected your [hit_area].", "Your armor has softened hit to your [hit_area].") //I guess "melee" is the best fit here

		if(armor < 2)
			apply_damage(throw_damage, dtype, zone, armor, is_sharp(O), has_edge(O), O)

		if(ismob(O.thrower))
			var/mob/M = O.thrower
			var/client/assailant = M.client
			if(assailant)
				src.attack_log += text("\[[time_stamp()]\] <font color='orange'>Has been hit with a [O], thrown by [M.name] ([assailant.ckey])</font>")
				M.attack_log += text("\[[time_stamp()]\] <font color='red'>Hit [src.name] ([src.ckey]) with a thrown [O]</font>")
				if(!istype(src,/mob/living/simple_animal/mouse))
					msg_admin_attack("[src.name] ([src.ckey]) was hit by a [O], thrown by [M.name] ([assailant.ckey]) (<A HREF='?_src_=holder;adminplayerobservecoodjump=1;X=[src.x];Y=[src.y];Z=[src.z]'>JMP</a>)")

		//thrown weapon embedded object code.
		if(dtype == BRUTE && istype(O,/obj/item))
			var/obj/item/I = O
			if (!I.is_robot_module())
				var/sharp = is_sharp(I)
				var/damage = throw_damage
				if (armor)
					damage /= armor+1

				//blunt objects should really not be embedding in things unless a huge amount of force is involved
				var/embed_chance = sharp? damage/I.w_class : damage/(I.w_class*3)
				var/embed_threshold = sharp? 5*I.w_class : 15*I.w_class

				//Sharp objects will always embed if they do enough damage.
				//Thrown sharp objects have some momentum already and have a small chance to embed even if the damage is below the threshold
				if((sharp && prob(damage/(10*I.w_class)*100)) || (damage > embed_threshold && prob(embed_chance)))
					affecting.embed(I)

		// Begin BS12 momentum-transfer code.
		if(O.throw_source && speed >= 15)
			var/obj/item/weapon/W = O
			var/momentum = speed/2
			var/dir = get_dir(O.throw_source, src)

			visible_message("\red [src] staggers under the impact!","\red You stagger under the impact!")
			src.throw_at(get_edge_target_turf(src,dir),1,momentum)

			if(!W || !src) return

			if(W.loc == src && W.sharp) //Projectile is embedded and suitable for pinning.
				var/turf/T = near_wall(dir,2)

				if(T)
					src.loc = T
					visible_message("<span class='warning'>[src] is pinned to the wall by [O]!</span>","<span class='warning'>You are pinned to the wall by [O]!</span>")
					src.anchored = 1
					src.pinned += O


/mob/living/carbon/human/proc/bloody_hands(var/mob/living/source, var/amount = 2)
	if (gloves)
		gloves.add_blood(source)
		gloves:transfer_blood = amount
		gloves:bloody_hands_mob = source
	else
		add_blood(source)
		bloody_hands = amount
		bloody_hands_mob = source
	update_inv_gloves()		//updates on-mob overlays for bloody hands and/or bloody gloves

/mob/living/carbon/human/proc/bloody_body(var/mob/living/source)
	if(wear_suit)
		wear_suit.add_blood(source)
		update_inv_wear_suit(0)
	if(w_uniform)
		w_uniform.add_blood(source)
		update_inv_w_uniform(0)

/mob/living/carbon/human/proc/handle_suit_punctures(var/damtype, var/damage)

	if(!wear_suit) return
	if(!istype(wear_suit,/obj/item/clothing/suit/space)) return
	if(damtype != BURN && damtype != BRUTE) return

	var/obj/item/clothing/suit/space/SS = wear_suit
	var/penetrated_dam = max(0,(damage - SS.breach_threshold)) // - SS.damage)) - Consider uncommenting this if suits seem too hardy on dev.

	if(penetrated_dam) SS.create_breaches(damtype, penetrated_dam)

